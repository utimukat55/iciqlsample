package ga.kasukasu.utility.IciqlSample.models;

import com.iciql.Iciql.IQColumn;
import com.iciql.Iciql.IQIndex;
import com.iciql.Iciql.IQIndexes;
import com.iciql.Iciql.IQSchema;
import com.iciql.Iciql.IQTable;
import com.iciql.Iciql.IndexType;
import java.io.Serializable;

@IQTable(name="MagazineCodeMaster")
public class Magazinecodemaster implements Serializable {

	private static final long serialVersionUID = 1L;

	@IQColumn(primaryKey=true, autoIncrement=true, nullable=false)
	public Integer id;

	@IQColumn(length=2000000000)
	public String changedmonth;

	@IQColumn(length=2000000000, nullable=false)
	public String createdate;

	@IQColumn(length=2000000000)
	public String description;

	@IQColumn(length=2000000000)
	public String establishedmonth;

	@IQColumn(length=2000000000)
	public String interval;

	@IQColumn(length=2000000000)
	public String kind;

	@IQColumn(nullable=false)
	public Integer magazinecode;

	@IQColumn(length=2000000000, nullable=false)
	public String magazinename;

	@IQColumn
	public Integer price;

	@IQColumn(length=2000000000)
	public String publishday;

	@IQColumn(length=2000000000)
	public String publishmonth;

	@IQColumn(length=2000000000)
	public String publisher;

	@IQColumn(length=2000000000)
	public String ruby;

	@IQColumn(length=2000000000)
	public String size;

	@IQColumn(length=2000000000)
	public String status;

	@IQColumn(length=2000000000)
	public String updatedate;

	public Magazinecodemaster() {
	}
}