package ga.kasukasu.utility.IciqlSample;

import java.time.OffsetDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iciql.Db;

import ga.kasukasu.utility.IciqlSample.models.Magazinecodemaster;

/**
 * Iciql usage.
 *
 */
public class App {
	private static Logger logger = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {
		logger.info("Hello Iciql/Sqlite!");
		try (Db db = Db.open("jdbc:sqlite:MagazineCodeJp.sqlite3")) {
			insert(db);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error("Interrupted", e);
			}
			select(db);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error("Interrupted", e);
			}
			delete(db);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error("Interrupted", e);
			}
			select(db);
		}
	}
	
	public static void insert(Db db) {
		logger.info("=== INSERT ===");
		Magazinecodemaster sample = new Magazinecodemaster();
		sample.magazinecode = 20401;
		sample.kind = "定期雑誌";
		sample.status = "稼働";
		sample.magazinename = "週刊文春";
		sample.ruby = "シュウカンブンシュン";
		sample.publisher = "（株）文藝春秋";
		sample.price = 389;
		sample.size = "B5";
		sample.publishmonth = "毎月";
		sample.publishday = "木";
		sample.interval = "週刊";
		sample.createdate = OffsetDateTime.now().toString();
		boolean result = db.insert(sample);
		logger.info("result of insert:[" + result + "]");
		logger.info("--- INSERT ---");
	}
	public static void select(Db db) {
		Magazinecodemaster table = new Magazinecodemaster();
		List<Magazinecodemaster> result = db.from(table).where(table.magazinecode).is(20401).select();

		logger.info("=== SELECT ===");
		logger.info(db.from(table).where(table.magazinecode).is(20401).toSQL());
		for (Magazinecodemaster  record : result) {
			logger.info("id:[" + record.id + "]");
			logger.info("magazineCode:[" + record.magazinecode + "]");
			logger.info("kind:[" + record.kind + "]");
			logger.info("status:[" + record.status + "]");
			logger.info("magazineName:[" + record.magazinename + "]");
			logger.info("ruby:[" + record.ruby + "]");
			logger.info("publisher:[" + record.publisher + "]");
			logger.info("price:[" + record.price + "]");
			logger.info("size:[" + record.size + "]");
			logger.info("publishMonth:[" + record.publishmonth + "]");
			logger.info("publishDay:[" + record.publishday + "]");
			logger.info("interval:[" + record.interval + "]");
			logger.info("establishedMonth:[" + record.establishedmonth + "]");
			logger.info("changedMonth:[" + record.changedmonth + "]");
			logger.info("description:[" + record.description + "]");
			logger.info("createDate:[" + record.createdate + "]");
			logger.info("updateDate:[" + record.updatedate + "]");
		}
		logger.info("--- SELECT ---");
	}
	public static void delete(Db db) {
		logger.info("=== DELETE ===");
		Magazinecodemaster sample = new Magazinecodemaster();
		int result = db.from(sample).where(sample.magazinecode).is(20401).delete();
		logger.info("result of delete:[" + result + "]");
		logger.info("--- DELETE ---");
	}
}
